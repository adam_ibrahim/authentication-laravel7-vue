<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginFormRequest;
use App\Http\Resources\UserResource;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api'])->except('login');
    }

    public function login(LoginFormRequest $request)
    {
        if(!$token = auth()->attempt($request->only('email', 'password'))) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }
        return response()->json(compact('token'));
    }

    /**
     * @return UserResource
     */
    public function me()
    {
        return new UserResource(auth()->user());
    }

    /**
     * Logout user
     */
    public function logout()
    {
        auth()->logout();
    }
}
