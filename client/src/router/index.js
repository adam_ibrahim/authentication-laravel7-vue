import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Dashboard from '../views/Dashboard.vue'
import Login from '../views/Login.vue'
import store from'@/store'

Vue.use(VueRouter)

  const routes = [
  {path: '/', name: 'Home', component: Home},
  {
    path: '/login',
    name: 'Login',
    component: Login,
    beforeEnter : (to, from, next) => {
      if(store.getters['auth/authenticated']) {
        return next({
          name:'dashboard'
        })
      }
      next()
    }
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    beforeEnter : (to, from, next) => {
      if(!store.getters['auth/authenticated']) {
        return next({
          name:'Login'
        })
      }
      next()
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
